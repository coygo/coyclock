package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"text/template"
	"time"
)

var date time.Time
var tpl *template.Template
var httpAddr string

const defaultTemplate = "{{.Year}}-{{.Month}}-{{.Day}} {{.Weekday}} {{.YearDay}}/{{.YearDays}} {{.YearProgress|printf \"%.2f\"}}% {{.WeekdayZH}} {{.Lunar}}"

func parseFlags() {
	var argDate string
	var argTpl string

	flag.StringVar(&argDate, "date", "now", "specific date in format YYYYMMDD")
	flag.StringVar(&argTpl, "tpl", defaultTemplate, "output template")
	flag.StringVar(&httpAddr, "http", "", "run as HTTP daemon (in format <host>:<port>)")
	flag.Parse()

	if argDate == "now" {
		date = time.Now().UTC()
	} else {
		var err error
		date, err = time.Parse("20060102", argDate)
		if err != nil {
			log.Fatal(err)
		}
	}

	tpl = template.Must(template.New("").Parse(argTpl))
}

func main() {
	parseFlags()

	if httpAddr == "" {
		tpl.Execute(os.Stdout, clock(&date))
		return
	}

	log.Println("HTTP serve on " + httpAddr)
	http.HandleFunc("/", clockHandler)
	http.HandleFunc("/json", jsonClockHandler)
	http.HandleFunc("/plain", textClockHandler)
	log.Fatal(http.ListenAndServe(httpAddr, nil))
}
