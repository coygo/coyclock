package main

import (
	"time"

	lunar "github.com/FrankWong1213/golang-lunar"
)

type result struct {
	Year         int     `json:"year"`
	Month        int     `json:"month"`
	Day          int     `json:"day"`
	Weekday      string  `json:"weekday"`
	WeekdayZH    string  `json:"weekday_zh"`
	YearDay      int     `json:"year_day"`
	YearDays     int     `json:"year_days"`
	YearProgress float64 `json:"year_progress"`
	Lunar        string  `json:"lunar"`
}

var weekdayX = map[time.Weekday]string{
	time.Sunday:    "星期日",
	time.Monday:    "星期一",
	time.Tuesday:   "星期二",
	time.Wednesday: "星期三",
	time.Thursday:  "星期四",
	time.Friday:    "星期五",
	time.Saturday:  "星期六",
}

func clock(t *time.Time) result {
	if t == nil {
		t = new(time.Time)
		*t = time.Now().UTC()
	}

	yearDay := t.YearDay()
	yearDays := yearDays(t.Year())
	return result{
		t.Year(),              // Year
		int(t.Month()),        // Month
		t.Day(),               // Day
		t.Weekday().String(),  // Weekday
		weekdayX[t.Weekday()], // WeekdayZH
		yearDay,
		yearDays,
		100 * float64(yearDay) / float64(yearDays), // YearProgress
		lunar.Lunar(t.Format("20060102")),          // Lunar
	}
}

func yearDays(year int) int {
	if isLeapYear(year) {
		return 366
	}
	return 365
}

func isLeapYear(year int) bool {
	if year%4 == 0 {
		if year%100 == 0 {
			if year%400 == 0 {
				return true
			}
			return false
		}
		return true
	}
	return false
}
