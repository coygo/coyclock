package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"text/template"
	"time"
)

func clockHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case http.MethodGet, http.MethodPost:
		if strings.HasPrefix(r.Header.Get("Accept"), "application/json") ||
			strings.HasPrefix(r.UserAgent(), "curl/") {
			jsonClockHandler(w, r)
			return
		}
		textClockHandler(w, r)
	default:
		msg := fmt.Sprintf("Method Not Allowed: %s", r.Method)
		http.Error(w, msg, http.StatusMethodNotAllowed)
	}
}

func jsonClockHandler(w http.ResponseWriter, r *http.Request) {
	var args url.Values
	var d *time.Time
	var isPretty bool

	switch r.Method {
	case http.MethodGet:
		args = r.URL.Query()
	case http.MethodPost:
		if err := r.ParseForm(); err != nil {
			log.Print(err)
			msg := fmt.Sprintf("ParseForm() error: %v", err)
			http.Error(w, msg, http.StatusInternalServerError)
			return
		}
		args = r.Form
	default:
		msg := fmt.Sprintf("Method Not Allowed: %s", r.Method)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}

	if a := args["date"]; a != nil {
		var err error
		argDate := a[len(a)-1]
		d = new(time.Time) // initial empty *time.Time
		if *d, err = time.Parse("20060102", argDate); err != nil {
			msg := fmt.Sprintf("Bad argument: date (%v)", err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
	}

	if a := args["pretty"]; a != nil {
		var err error
		argPretty := a[len(a)-1]
		if isPretty, err = strconv.ParseBool(argPretty); err != nil {
			isPretty = false
		}
	}

	var b []byte
	var err error
	if !isPretty {
		b, err = json.Marshal(clock(d))
	} else {
		b, err = json.MarshalIndent(clock(d), "", "    ")
	}
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Add("content-type", "application/json; charset=utf-8")
	if isPretty {
		b = append(b, '\n')
	}
	w.Write(b)
}

func textClockHandler(w http.ResponseWriter, r *http.Request) {
	var args url.Values
	var d *time.Time
	var t = tpl // *template.Template

	switch r.Method {
	case http.MethodGet:
		args = r.URL.Query()
	case http.MethodPost:
		if err := r.ParseForm(); err != nil {
			msg := fmt.Sprintf("ParseForm() err: %v", err)
			http.Error(w, msg, http.StatusInternalServerError)
			return
		}
		args = r.Form
	default:
		msg := fmt.Sprintf("Method Not Allowed: %s", r.Method)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}

	if a := args["date"]; a != nil {
		var err error
		argDate := a[len(a)-1]
		d = new(time.Time) // initial empty *time.Time
		if *d, err = time.Parse("20060102", argDate); err != nil {
			msg := fmt.Sprintf("Bad argument: date (%v)", err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
	}

	if a := args["tpl"]; a != nil {
		var err error
		argTpl := a[len(a)-1]
		if t, err = template.New("").Parse(argTpl); err != nil {
			msg := fmt.Sprintf("Bad argument: tpl (%v)", err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
	}

	t.Execute(w, clock(d))
}
